function showNavMobile(){
    document.getElementById('nav-mobile').style.visibility = 'visible';
    document.getElementById('ham-button').style.visibility = 'hidden';
    document.getElementById('close-button').style.visibility = 'visible';
}

function hideNavMobile(){
    document.getElementById('nav-mobile').style.visibility = 'hidden';
    document.getElementById('ham-button').style.visibility = 'visible';
    document.getElementById('close-button').style.visibility = 'hidden';
}

